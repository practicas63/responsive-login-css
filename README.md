# responsive-login-css
A basic login interface was developed with CSS and a responsive design that adjusts to 3 different screen sizes, showing a login form with its fields and buttons to enter by other means.

FULL SCREEN:
![full screen](img/full-screen.png);

MEDIUM SCREEN:
![full screen](img/medium-screen.png);

SMALL SCREEN:
![full screen](img/small-screen.png);